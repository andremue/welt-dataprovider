'use strict';

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('config.json');
const db = low(adapter);
const _ = db._;

db.defaults({
  datasources: [],
  stateData: [{
    id: 0,
    data: {
      house: { total: {} },
      senate: { total: {} }
    }
  }]
})
  .write();

const database = {
  getDatasources: () => {
    let res = db.get('datasources')
      .value();
    return res;
  },

  addDatasource: newDatasource => {
    console.log(newDatasource);
    db.get('datasources')
      .push(newDatasource)
      .write();
  },

  removeDataSource: datasourceID => {
    db.get('datasources')
      .remove({id: datasourceID})
      .write();
  },

  addStateData: data => {
    let id = new Date().toISOString();
    db.get('stateData')
      .push({id, data})
      .write();
  },

  getStateDataIDs: () => {
    let res = db.get('stateData')
      .map('id')
      .value();
    return res;
  },

  getLastStateData: () => {
    let res = db.get('stateData')
      .map('data')
      .sortBy(_.last)
      .reverse()
      .take(1)
      .value();
    return res[0];
  },

  getStateData: (id) => {
    let res = db.get('stateData')
      .find({ id })
      .value();
    return res;
  }
};

module.exports = database;
