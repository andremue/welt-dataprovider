'use strict';

const DOMParser = require('xmldom').DOMParser;
const request = require('superagent');
const xpath = require('xpath');

class MSEClient {
  constructor (mse) {
    this.uris = {
      mse,
      directory: undefined,
      profiles: undefined,
      shows: undefined,
      playlists: undefined
    };
    this.select = xpath.useNamespaces({
      viz: 'http://www.vizrt.com/atom',
      atom: 'http://www.w3.org/2005/Atom',
      vdf: 'http://www.vizrt.com/types',
      app: 'http://www.w3.org/2007/app',
      vaext: 'http://www.vizrt.com/atom-ext'
    });
  }

  async makeRequest (uri, path) {
    try {
      const res = await request.get(uri).timeout({response: 5000}).buffer();
      if (res.status !== 200) {
        throw new Error(`Wrong response status: ${res.status}`);
      }
      const dom = await new DOMParser().parseFromString(res.text);
      const result = this.select(path, dom);
      return result;
    } catch (error) {
      console.log(`An error occured in makeRequest(): ${error.message}`);
    }
  };

  async initialize (callback) {
    try {
      if (this.uris.directory === undefined || this.uris.shows === undefined) {
        console.log('Initalization needed...');
        let path = '//app:service/app:workspace/app:collection[app:categories/atom:category[@term="directory"]]/@href';
        let res = await this.makeRequest(this.uris.mse, path);
        if (!res) {
          throw new Error('Empty response from request.');
        }
        this.uris.directory = res[0].value;

        path = '//app:service/app:workspace/app:collection[app:categories/atom:category[@term="profile"]]/@href';
        res = await this.makeRequest(this.uris.mse, path);
        if (!res) {
          throw new Error('Empty response from request.');
        }
        this.uris.profiles = res[0].value;

        path = '//atom:feed/atom:entry[atom:category[@term="directory"] and atom:title[text()="shows"]]/atom:link[@rel="alternate"]/@href';
        res = await this.makeRequest(this.uris.directory, path);
        if (!res) {
          throw new Error('Empty response from request.');
        }
        this.uris.shows = res[0].value;

        path = '//atom:feed/atom:entry[atom:category[@term="directory"] and atom:title[text()="playlists"]]/atom:link[@rel="alternate"]/@href';
        res = await this.makeRequest(this.uris.directory, path);
        if (!res) {
          throw new Error('Empty response from request.');
        }
        this.uris.playlists = res[0].value;

        console.log('directoryUri: ', this.uris.directory);
        console.log('profileUri: ', this.uris.profiles);
        console.log('directoryShowUri: ', this.uris.shows);
        console.log('directoryPlaylistUri: ', this.uris.playlists);
        callback();
      }
    } catch (error) {
      console.log(`An error occured in initialize(): ${error.message}`);
    }
  };

  async getShows () {
    try {
      await this.initialize();
      let path = `/atom:feed/atom:entry/atom:title/text() | /atom:feed/atom:entry/atom:link[@rel="alternate"]/@href`;
      const res = await this.makeRequest(this.uris.shows, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }
      let showTitleList = [];
      for (let i = 0; i < res.length; i += 2) {
        showTitleList.push({
          name: res[i].nodeValue,
          id: encodeURI(res[i].nodeValue),
          link: res[i + 1].nodeValue
        });
      }
      return showTitleList;
    } catch (error) {
      console.log(`An error occured in getShows(): ${error.message}`);
    }
  };

  async getShow (showName) {
    try {
      await this.initialize();
      let path = `//atom:feed/atom:entry[atom:title[text()="${showName}"]]/atom:link[@rel="alternate"]/@href`;
      let res = await this.makeRequest(this.uris.shows, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }
      const showUri = res[0].nodeValue;

      path = `//atom:feed/atom:entry[atom:category[@term='element_collection']]/atom:link[@rel='alternate']/@href`;
      res = await this.makeRequest(showUri, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }
      const elementCollectionUri = res[0].nodeValue;

      path = `//atom:entry/atom:title | //atom:entry/atom:summary`;
      res = await this.makeRequest(elementCollectionUri, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }

      let show = {};
      show.elements = [];
      for (let i = 0; i < res.length; i += 2) {
        let name = '';
        let summary = '';
        if (res[i].firstChild) {
          name = res[i].firstChild.nodeValue;
        }
        if (res[i + 1].firstChild) {
          summary = res[i + 1].firstChild.nodeValue;
        }
        show.elements.push({name, summary});
      }

      console.log('showUri ', showUri);
      console.log('elementCollectionUri ', elementCollectionUri);
      return show;
    } catch (error) {
      console.log(`An error occured in getShow(): ${error.message}`);
    }
  }

  async getPlaylists () {
    try {
      await this.initialize();
      let path = `//atom:feed/atom:entry/atom:title`;
      let res = await this.makeRequest(this.uris.playlists, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }

      return res.map(title => title.childNodes[0].nodeValue);
    } catch (error) {
      console.log(`An error occured in getPlaylists(): ${error.message}`);
    }
  };

  async getProfiles () {
    try {
      await this.initialize();
      let path = `//atom:feed/atom:entry/atom:title/text() | /atom:feed/atom:entry/atom:link[@rel="alternate"]/@href`;
      const res = await this.makeRequest(this.uris.profiles, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }
      let profileTitleList = [];
      for (let i = 0; i < res.length; i += 2) {
        profileTitleList.push({
          name: res[i].nodeValue,
          id: encodeURI(res[i].nodeValue),
          link: res[i + 1].nodeValue
        });
      }
      return profileTitleList;
    } catch (error) {
      console.log(`An error occured in getProfiles(): ${error.message}`);
    }
  };

  async getProfile (profileName) {
    try {
      await this.initialize();

      // get the profile uri of the profile with the given name
      let path = `//atom:feed/atom:entry[atom:title[text()="${profileName}"]]/atom:link[@rel="alternate"]/@href`;
      let res = await this.makeRequest(this.uris.profiles, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }

      const profileUri = res[0].nodeValue;

      // get the profile data
      path = `//vdf:list/vdf:payload/vdf:field[@name='name']/vdf:value/text()`;
      res = await this.makeRequest(profileUri, path);
      if (!res) {
        throw new Error('Empty response from request.');
      }

      let profile = {
        profileName,
        channels: []
      };

      for (let i = 0; i < res.length; i++) {
        let channel = {
          name: res[i].nodeValue
        };

        // get channel data
        let pathChannelSelect = `//vdf:list/vdf:payload[vdf:field[@name='name']/vdf:value/text() = "${channel.name}"]`;
        path = `${pathChannelSelect}/vdf:field[@name='video-role']/vdf:value/text() |
          ${pathChannelSelect}/vdf:field[@name='viz-role']/vdf:value/text() |
          ${pathChannelSelect}/vdf:field[@name='outputs']/vdf:list/vdf:payload/vdf:field/vdf:value/text()`;
        let resChannel = await this.makeRequest(profileUri, path);
        if (!resChannel) {
          throw new Error('Empty response from request.');
        }

        channel.output = [];

        for (let j = 0; j < resChannel.length; j++) {
          // Estimate the node data type
          let nodeName = resChannel[j].parentNode.parentNode.attributes[0].nodeValue;
          let nodeValue = resChannel[j].nodeValue;
          if (nodeName === 'output') {

            path = `//vdf:list/vdf:payload/vdf:field[@name='host']/vdf:value/text() |
              //vdf:field[@name='connected']/vdf:value/text() |
              //vdf:field[@name='status']/vdf:value/text()`;
            let tmp = await this.makeRequest(resChannel[j].nodeValue, path);

            let output = {};
            for (let k = 0; k < tmp.length; k++) {
              let name = tmp[k].parentNode.parentNode.attributes[0].nodeValue;
              let val = tmp[k].nodeValue;
              output[name] = val;
            }
            channel.output.push(output);
          } else {
            let nodeNameCamel = nodeName.replace(/-([a-z])/gi, (s, group1) => {
              return group1.toUpperCase();
            });
            channel[nodeNameCamel] = nodeValue;
          }
        }
        profile.channels.push(channel);
      }

      return profile;
    } catch (error) {
      console.log(`An error occured in getProfile(): ${error.message}`);
    }
  }
};

module.exports = MSEClient;
