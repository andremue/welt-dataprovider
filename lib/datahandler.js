'use strict';

const path = require('path');
const xlsx = require('xlsx-populate');
const _ = require('lodash');

const database = require('./database');

class Datahandler {
  constructor (apClient, XLSX_PATH) {
    this.apClient = apClient;
    this.excelFile = undefined;
    this.stateData = {
      house: { total: {} },
      senate: { total: {} }
    };
    this.xlsxPath = XLSX_PATH;
  }

  async updateData (provider, destination, parameter) {
    switch (provider) {
      case 'AP':
        this.stateData = await database.getLastStateData();
        let stateData = _.cloneDeep(this.stateData);

        // TOTAL DATA
        let res = await this.apClient.updateData(2);
        let json = res.body;

        json.reports.forEach(async report => {
          let [type, subtype, test, level] = report.title.split('/').map(item => item.trim());
          if (!level) {
            [level, test] = [test, 'false'];
          }

          // Get the content
          res = await this.apClient.makeRequest(report.contentLink);
          res.body.trendtable.party.forEach(element => {
            let trend = element.trend.reduce((accumulator, obj) => ({...accumulator, ...obj}));

            switch (subtype) {
              case 's':
                stateData.senate.total[element.title + 'Won'] = trend['Won'];
                stateData.senate.total[element.title + 'Leading'] = trend['Leading'];
                break;

              case 'h':
                stateData.house.total[element.title + 'Won'] = trend['Won'];
                stateData.house.total[element.title + 'Leading'] = trend['Leading'];
                break;
            }
          });
        });

        // STATE DATA
        res = await this.apClient.updateData(1);
        json = res.body;

        json.races.forEach(race => {
          if (race.reportingUnits) {
            let reporting = race.reportingUnits[0];
            let statePostal = reporting.statePostal;
            if (race.officeID === 'H') {
              if (!stateData.house.hasOwnProperty(statePostal)) {
                stateData.house[statePostal] = {
                  name: reporting.stateName,
                  reported: 0,
                  total: 0,
                  percent: 0,
                  votes: {
                    DemWon: 0,
                    DemLeading: 0,
                    GOPWon: 0,
                    GOPLeading: 0,
                    OthersWon: 0,
                    OthersLeading: 0
                  }
                };
              };
              stateData.house[statePostal].total = 0;
              stateData.house[statePostal].reported = 0;
              stateData.house[statePostal].votes.DemWon = 0;
              stateData.house[statePostal].votes.DemLeading = 0;
              stateData.house[statePostal].votes.GOPWon = 0;
              stateData.house[statePostal].votes.GOPLeading = 0;
              stateData.house[statePostal].votes.OthersWon = 0;
              stateData.house[statePostal].votes.OthersLeading = 0;
            };
          }
        });

        json.races.forEach(race => {
          if (race.reportingUnits) {
            let description = race.description;
            let reporting = race.reportingUnits[0];
            let candidates = reporting.candidates;
            let statePostal = reporting.statePostal;

            // Remove not needed properties from the candidates object
            candidates.forEach(candidate => {
              delete candidate.candidateID;
              delete candidate.polID;
              delete candidate.ballotOrder;
              delete candidate.polNum;
              delete candidate.abbrv;
            });

            // Sorting criteria
            // 1) winner
            // 2) votes (descending)
            // 3) ballot order (ascending)
            candidates.sort((a, b) => {
              if (a.winner) return -1;
              if (a.voteCount > b.voteCount) return -1;
              if (a.ballotOrder < b.ballotOrder) return -1;
              return 1;
            });

            switch (race.officeID) {
              case 'H':
                stateData.house[statePostal].total++;

                // We have already a winner - only the winner candidate matters
                if (candidates[0].winner === 'X') {
                  stateData.house[statePostal].reported++;
                  stateData.house[statePostal].percent = stateData.house[statePostal].reported / stateData.house[statePostal].total * 100;
                  if (candidates[0].party === 'Dem') {
                    stateData.house[statePostal].votes.DemWon++;
                  } else if (candidates[0].party === 'GOP') {
                    stateData.house[statePostal].votes.GOPWon++;
                  } else {
                    stateData.house[statePostal].votes.OthersWon++;
                  }
                } else {
                  if (candidates[0].party === 'Dem') {
                    stateData.house[statePostal].votes.DemLeading++;
                  } else if (candidates[0].party === 'GOP') {
                    stateData.house[statePostal].votes.GOPLeading++;
                  } else {
                    stateData.house[statePostal].votes.OthersLeading++;
                  }
                }
                break;

              case 'S':

                if (description === 'Class II') {
                  statePostal = statePostal + '2';
                };

                stateData.senate[statePostal] = {
                  name: reporting.stateName,
                  reported: reporting.precinctsReporting,
                  total: reporting.precinctsTotal,
                  percent: reporting.precinctsReportingPct
                };
                stateData.senate[statePostal].candidates = candidates;
                stateData.senate[statePostal].votesTotal = candidates.reduce((pv, cv) => pv + cv.voteCount, 0);
                break;
            }
          }
        });
        console.log('\n');
        console.log(new Date().toISOString());
        // WRITE DATA TO DATABASE
        if (_.isEqual(stateData, this.stateData)) {
          console.log('No new data!');
        } else {
          this.addStateData(stateData);
          console.log('New data written to database.');
        }
        break;
    }
    return true;
  };

  async writeExcelFile (stateData) {
    let cellMapping = {
      senate: {
        total: {
          sheet: 'SENAT',
          cells: {
            DemWon: 'D11',
            DemLeading: 'D13',
            GOPWon: 'D14',
            GOPLeading: 'D16',
            OthersWon: 'D17',
            OthersLeading: 'D18'
          }
        },
        states: {
          sheet: 'SENAT_EINZEL',
          cells: {
            stateName: 'C7',
            abbreviation: 'C8',
            percent: 'C10',
            incumbentName: 'C11',
            incumbentParty: 'C12',
            candidate: [
              {name: 'C14', party: 'C15', votes: 'C16', winner: 'C17'},
              {name: 'D14', party: 'D15', votes: 'D16', winner: 'D17'},
              {name: 'E14', party: 'E15', votes: 'E16', winner: 'E17'}
            ]
          }
        }
      },
      house: {
        total: {
          sheet: 'HAUS',
          cells: {
            DemWon: 'D11',
            DemLeading: 'D13',
            GOPWon: 'D14',
            GOPLeading: 'D16',
            OthersWon: 'D17',
            OthersLeading: 'D18'
          }
        },
        states: {
          sheet: 'HAUS_EINZEL',
          cells: {
            stateName: 'C7',
            abbreviation: 'C8',
            percent: 'C10',
            votes: {
              DemWon: 'D15',
              DemLeading: 'D17',
              GOPWon: 'D18',
              GOPLeading: 'D20',
              OthersWon: 'D21',
              OthersLeading: 'D22'
            }
          }
        }
      }
    };

    var workbook = await xlsx.fromFileAsync(path.join(__dirname, '../public/US2018_GESAMT.xlsx'));

    for (let officeID in cellMapping) {
      console.log('officeID: ', officeID);
      for (let level in stateData.data[officeID]) {
        let mapping;
        let sheetName;
        if (level === 'total') {
          mapping = cellMapping[officeID].total;
          sheetName = mapping.sheet;
          for (let party in mapping.cells) {
            workbook.sheet(sheetName).cell(mapping.cells[party]).value(parseInt(stateData.data[officeID][level][party], 10));
          }
          workbook.toFileAsync(path.join(__dirname, this.xlsxPath, sheetName + '_' + level + '.xlsx'));
        } else {
          mapping = cellMapping[officeID].states;
          sheetName = mapping.sheet;
          workbook.sheet(sheetName).cell(mapping.cells.stateName).value(stateData.data[officeID][level].name);
          workbook.sheet(sheetName).cell(mapping.cells.abbreviation).value(level);
          workbook.sheet(sheetName).cell(mapping.cells.percent).value(stateData.data[officeID][level].percent);
          if (officeID === 'senate') {
            for (let i = 0; i < 3; i++) {
              if (!stateData.data[officeID][level].candidates[i]) {
                stateData.data[officeID][level].candidates[i] = {
                  party: '',
                  first: '',
                  last: '',
                  voteCount: 0
                };
              }
              let candidate = stateData.data[officeID][level].candidates[i];

              workbook.sheet(sheetName).cell(mapping.cells.candidate[i].party).value(candidate.party);
              workbook.sheet(sheetName).cell(mapping.cells.candidate[i].name).value(candidate.first + ' ' + candidate.last);
              workbook.sheet(sheetName).cell(mapping.cells.candidate[i].votes).value(Math.round(candidate.voteCount / stateData.data[officeID][level].votesTotal * 10000) / 100);
              if (candidate.winner) {
                workbook.sheet(sheetName).cell(mapping.cells.candidate[i].winner).value('Ja');
              }

              if (candidate.incumbent) {
                workbook.sheet(sheetName).cell(mapping.cells.incumbentName).value(candidate.first + ' ' + candidate.last);
                workbook.sheet(sheetName).cell(mapping.cells.incumbentParty).value(candidate.party);
              }
            }
          } else {
            for (let party in mapping.cells.votes) {
              workbook.sheet(sheetName).cell(mapping.cells.votes[party]).value(parseInt(stateData.data[officeID][level]['votes'][party], 10));
            }
          }
          workbook.activeSheet(sheetName);
          workbook.toFileAsync(path.join(__dirname, this.xlsxPath, sheetName + '_' + level + '.xlsx'));
        }
      }
    }
    console.log('Writing files finished');
    return true;
  }

  async addStateData (newStateData) {
    database.addStateData(newStateData);
  };

  async getStateDataIDs () {
    return database.getStateDataIDs();
  };

  saveToFile (id) {
    let stateData = database.getStateData(id);
    this.writeExcelFile(stateData);
  };

  async addDatasource (newDatasource) {
    database.addDatasource(newDatasource);
  };

  async getDatasources () {
    return database.getDatasources();
  };

  async removeDatasource (id) {
    database.removeDataSource(id);
  };
}

module.exports = Datahandler;
