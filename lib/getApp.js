'use strict';

const serve = require('koa-static');
const mount = require('koa-mount');
const Koa = require('koa');
const Router = require('koa-router');

const api = new Koa();
const app = new Koa();
const router = new Router();

const getApp = function (datahandler) {
  // todo: GET -> POST because of UPDATING the data
  // todo: new GET that only returns the current data set
  router.get('/ap/midtermElection2018', async (ctx, next) => {
    await datahandler.updateData('AP', 'XLSX');
    ctx.body = 'FINISHED';
  });

  /*
  router.get('/shows', async (ctx, next) => {
    const shows = await mse.getShows();
    console.log(shows);
    ctx.body = { shows };
  });

  router.post('/shows', async (cty, next) => {

  });

  router.put('/shows/:id', async (ctx, next) => {
    console.log('Add new show with id ', ctx.params.id);
  });

  router.del('/shows/:id', async (ctx, next) => {
    console.log('Delete show with id ', ctx.params.id);
  });
*/
  api
    .use(router.routes())
    .use(router.allowedMethods());

  app.use(mount('/', serve('public')));
  app.use(mount('/api', api));

  return app;
};

module.exports = getApp;
