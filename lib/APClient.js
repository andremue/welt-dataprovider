'use strict';

const request = require('superagent');

let RequestType = Object.freeze({
  ELECTIONS: 1,
  REPORTS: 2
});

class APClient {
  constructor (apiKey, electionDate, version = 'v2', format = 'json', test = true) {
    this.apiKey = apiKey;
    this.electionDate = electionDate;
    this.version = version;
    this.format = format;
    this.test = test;
    this.nextUris = {
      elections: `https://api.ap.org/${this.version}/elections/${this.electionDate}?test=${this.test}&format=${this.format}&officeID=S,H`,
      reports: `https://api.ap.org/${this.version}/reports/?test=${this.test}&format=${this.format}&type=trend&subtype=S,H&electionDate=${this.electionDate}`
    };
  };

  async updateData (requestType) {
    let data;
    switch (requestType) {
      case RequestType.ELECTIONS:
        // console.log('UPDATE ELECTION DATA');
        data = await this.makeRequest(this.nextUris.elections);
        this.nextUris.elections = data.body.nextrequest;
        break;
      case RequestType.REPORTS:
        // console.log('UPDATE REPORT DATA');
        data = await this.makeRequest(this.nextUris.reports);
        // console.log(data.body.nextrequest);
        this.nextUris.reports = data.body.nextrequest;
        break;
    }
    return data;
  }

  async makeRequest (uri) {
    let parameters = `?apiKey=${this.apiKey}`;
    if (uri.includes('?')) {
      parameters = parameters.replace('?', '&');
    }
    if (!uri.includes('format=')) {
      parameters = parameters + `&format=${this.format}`;
    }
    // console.log('REQUEST TO: ', uri + parameters);
    try {
      const res = await request.get(uri + parameters).timeout({response: 10000}).buffer();
      if (res.status !== 200) {
        throw new Error(`Wrong response status: ${res.status}`);
      }
      return res;
    } catch (error) {
      console.log(`An error occured in makeRequest(): ${error.message}`);
    }
  };
}

module.exports = APClient;
