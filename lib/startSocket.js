'use strict';

const socket = require('socket.io');

const startSocket = function (server, datahandler) {
  const io = socket(server);

  io.on('connection', socket => {

    socket.on('ADD DATASOURCE', async newDatasource => {
      await datahandler.addDatasource(newDatasource);
      let datasources = await datahandler.getDatasources();
      io.emit('DATASOURCES', datasources);
    });

    socket.on('GET DATASOUCRES', async () => {
      let datasources = await datahandler.getDatasources();
      io.emit('DATASOURCES', datasources);
    });

    socket.on('REMOVE DATASOURCE', async id => {
      await datahandler.removeDatasource(id);
      let datasources = await datahandler.getDatasources();
      io.emit('DATASOURCES', datasources);
    });

    socket.on('GET DATASETS', async () => {
      let datasets = await datahandler.getStateDataIDs();
      io.emit('DATASETS', datasets);
    });

    socket.on('SAVE TO FILE', async id => {
      await datahandler.saveToFile(id);
    });

  });
};

module.exports = startSocket;
