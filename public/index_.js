let socket = io();
let app;

let data = {
  newDatasource: {
    name: '',
    id: '',
    dataprovider: '',
    output: ''
  },
  currentDatasource: {},
  datasources: []
};

socket.on('DATASOURCES', datasources => {
  data.datasources = datasources;
});

app = new Vue({
  el: '#app',
  data,
  methods: {
    saveNewDatasource: function () {
      socket.emit('ADD DATASOURCE', this.currentDatasource);
    },

    removeDatasource: function (id) {
      socket.emit('REMOVE DATASOURCE', id);
    }
  },
  created: function () {
    socket.emit('GET DATASOUCRES', '');
  }
});
