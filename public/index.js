let socket = io();
let app;

let data = {
  datasets: []
};

socket.on('DATASETS', datasets => {
  data.datasets = datasets;
});

app = new Vue({
  el: '#app',
  data,
  methods: {
    saveToFile: function (id) {
      socket.emit('SAVE TO FILE', id);
    }
  },
  created: function () {
    socket.emit('GET DATASETS', '');
  }
});
