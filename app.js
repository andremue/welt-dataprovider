'use strict';

const http = require('http');

const processenv = require('processenv');

const APClient = require('./lib/APClient');
const Datahandler = require('./lib/datahandler');
const getApp = require('./lib/getApp');
const startSocket = require('./lib/startSocket');

const PORT = processenv('PORT', 8080);
const UPDATE_TIMER = processenv('UPDATE_TIMER', 1);
const TESTING = processenv('TESTING', true);
const XLSX_PATH = processenv('XLSX_PATH', '../out/');
// const MSE_PORT = processenv('MSE_PORT', 8580);
// const MSE_HOST = processenv('MSE_HOST', 'localhost');
// const MSE_PROTOCOL = processenv('MSE_PROTOCOL', 'http');

const apClient = new APClient('SuVy9rmjEWM98csBtXdT2MwqRa9jZuGD', '2018-11-06', 'v2', 'json', TESTING);
const datahandler = new Datahandler(apClient, XLSX_PATH);

async function updateData () {
  let result = await datahandler.updateData('AP', 'XLSX');
  console.log(result);
  setTimeout(updateData, 60000 * UPDATE_TIMER);
}

updateData();

const app = getApp(datahandler);

const server = http.createServer(app.callback());

startSocket(server, datahandler);

server.listen(PORT, () => {
  console.log(`welt-dataprovide is running on ${PORT}...`);
});
